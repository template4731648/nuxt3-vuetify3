import { defineStore } from "pinia"
import type { _ActionsTree, _GettersTree } from "pinia"
export const useAdminStore = defineStore("admin", () => {
  //state
  // const count = ref(0)
  const isLoggedIn = ref(true)
  const hasPermission = ref(true)
  //action
  // const increment = () => {
  //   count.value += 1
  // }
  // const decrement = () => {
  //   count.value -= 1
  // }
  //getters
  // const doubleCount = computed(() => count.value * 2)

  return {
    isLoggedIn,
    hasPermission,
    // count,
    // increment,
    // decrement,
    // doubleCount,
  }
})
